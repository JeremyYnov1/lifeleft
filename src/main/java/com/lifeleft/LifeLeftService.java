package com.lifeleft;
import java.util.Calendar;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(serviceName = "LifeLeft")
public class LifeLeftService {
    private static final Integer ESPERANCE_VIE_HOMME = 79;
    private static final Integer ESPERANCE_VIE_FEMME = 85;

    String homme = "homme";
    String femme = "femme";

    Integer esperance = 0;

    @WebMethod
    public String anneesRestantesAVivre(String prenom, String type, int anneeDeNaissance) {
        if (type == "homme"){
            this.esperance = ESPERANCE_VIE_HOMME;
        } else {
            this.esperance = ESPERANCE_VIE_FEMME;
        }

        Integer anneesRestantes = esperance - (Calendar.getInstance().get(Calendar.YEAR) - anneeDeNaissance);

        return "Bonjour " + prenom + ", il vous reste " + anneesRestantes + " ans a vivre.";
    }

}
